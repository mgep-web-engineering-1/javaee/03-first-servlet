# My First Servlet

## Make it work

* Clone the repository
* Open folder with VSCode.
* Right click and Run ```src > main > java > ... > Main.java```.
* Open http://localhost:8080 in the browser.

## Objective

This project will show how to manage request directly in java files.

Usually, when you are managing HTTP requests, you want to have control over the request (check user permissions, redirect if something happends...). In order to do that, java files could handle the requests and more advanced interactions can be performed.

* We will send the form parameter to a ```Java Servlet``` instead of a ```JSP file```.
  * Access to http://localhost:8080, we will see the form.
  * Fill the form with the text *something* and send it.
  * A 2nd request will be made automatically, this time to http://localhost:8080/main.
    * See that we are not making a request to a ```jsp``` file
  * *something* will show in a new page.
  * If we enter the 2nd url directly **I couldn't get any message** will be shown.

## Explaination

* The form action inside ```index.jsp``` is just ```main``` instead of a JSP file.
* If we open ```src > main > java > controller > MainController.java``` file, we can find this annotation:

```java
import jakarta.servlet.annotation.WebServlet;
...
@WebServlet(
    name = "MainController",
    urlPatterns = {"/main"}
)
public class MainController extends HttpServlet {
...
```

This is telling tomcat that if somebody calls to ```/main``` path (or URL), this Servlet will respond.

```MainController``` servlet has a constructor and 2 functions:

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    ...
}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
}
```

* ```doGet``` is executed when a GET request is received.
* ```doPost``` is executed when a POST request is received.
  * In this case and to simplify things, as ```doPost``` calls ```doGet``` directly, ```doGet``` is allways executed.
  * In real case scenarios this propably may change.

Our servlet's ```doGet``` implementation does 2 main things:

* (1) Get the parameter from the request.
* (2) Create an HTML with the parameter in an H1 tag.
  * **This is a simple example, but generating HTML directly on the servlet is not recommended! HTML should not be written in a Servlet**.
  * We will se how it should be used in future repositories.

```java
response.setContentType("text/html");
String message = request.getParameter("message");
PrintWriter out = response.getWriter();
if(message==null) {
    out.println("<h1>I couldn't get any message</h1>");
}else {
    out.println("<h1>".concat(message).concat("</h1>"));
}
```

### POST Vs. GET

* If we use ```GET```, parameters are sent within the path (or URL).
* If we use ```POST```, the parameters are sent within the body of the request and are not visible in the path.
  * It is usually safer to use ```POST``` to send information (more if you are sending passwords for example)
  * It also allows larger content as URLs are limited in length.

## Trying and understanding

Try to modify the code to work the same way but changing when the servlet/controller is being called (e.g.: http://localhost:8080/mainController).

Try to print also which has been used is (```GET``` or ```POST```).

## Exercise

* Create index.html
* It will have 3 forms:
  * (1) Will send 'name', 'second-name' & 'email' parameters to ```user.jsp```. ```user.jsp``` will show those parameters in a table:

| | |
|-------------|----------------------|
| name        | ALAIN                |
| second-name | PEREZ                |
| email       | aperez@mondragon.edu |

  * (2) Will call ExampleController to ```/example``` path using GET method, and the controller will print a H1 with ```GET``` text.
  * (3) Will call ExampleController to ```/example``` path using POST method, and the controller will print a H1 with ```POST``` text.

## Next and before

* Before [02-retrieve-parameters](https://gitlab.com/mgep-web-engineering-1/javaee/02-retrieve-parameters)
* Next [04-servlet-redirect](https://gitlab.com/mgep-web-engineering-1/javaee/04-servlet-redirect)