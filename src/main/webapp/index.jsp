<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>First Servlet in JSP</title>
	</head>
	<body>
		<h1>Retrieving parameters in JSP</h1>
		<form action="main" method="post">
			<label>Write something to send: <input type="text" name="message"/></label><br/>
			<button type="submit">Send</button>
		</form>
	</body>
</html>